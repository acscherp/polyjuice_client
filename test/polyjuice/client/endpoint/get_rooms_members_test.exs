# Copyright 2021 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomsMembersTest do
  use ExUnit.Case

  test "GET rooms/{room_id}/members" do
    endpoint = %Polyjuice.Client.Endpoint.GetRoomsMembers{
      room: "!room_id",
      at: nil,
      membership: nil,
      not_membership: nil
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: "{}",
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"}
             ],
             method: :get,
             path: "_matrix/client/r0/rooms/%21room_id/members"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({
        "chunk":
        [{
          "type":"m.room.member",
          "room_id":"room:example.org",
          "sender":"alice:example.org",
          "content":{"membership":"join","displayname":"alice"},
          "state_key":"@alice:example.org",
          "origin_server_ts":1630350687144,
          "event_id":"aabbccddee",
          "user_id":"@alice:example.org",
          "age":53178703
        }]
          })
           ) ==
             {:ok,
              %{
                "chunk" => [
                  %{
                    "age" => 53_178_703,
                    "content" => %{"displayname" => "alice", "membership" => "join"},
                    "event_id" => "aabbccddee",
                    "origin_server_ts" => 1_630_350_687_144,
                    "room_id" => "room:example.org",
                    "sender" => "alice:example.org",
                    "state_key" => "@alice:example.org",
                    "type" => "m.room.member",
                    "user_id" => "@alice:example.org"
                  }
                ]
              }}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
