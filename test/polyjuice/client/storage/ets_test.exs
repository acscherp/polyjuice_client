# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Storage.EtsTest do
  use ExUnit.Case
  alias Polyjuice.Client.Storage
  alias Polyjuice.Client.Filter

  test "ets can store sync token" do
    ets = Storage.Ets.open()

    assert Storage.get_sync_token(ets) == nil

    Storage.set_sync_token(ets, "token")
    assert Storage.get_sync_token(ets) == "token"

    Storage.set_sync_token(ets, "token2")
    assert Storage.get_sync_token(ets) == "token2"

    Storage.close(ets)
  end

  test "ets can store filters" do
    ets = Storage.Ets.open()

    assert Storage.get_filter_id(ets, %{}) == nil

    Storage.set_filter_id(ets, %{}, "filterid1")
    assert Storage.get_filter_id(ets, %{}) == "filterid1"

    assert Storage.get_filter_id(ets, Filter.lazy_loading()) == nil

    Storage.set_filter_id(ets, Filter.lazy_loading(), "filterid2")
    assert Storage.get_filter_id(ets, Filter.lazy_loading()) == "filterid2"
    assert Storage.get_filter_id(ets, %{}) == "filterid1"
  end

  test "ets can store key-values" do
    ets = Storage.Ets.open()

    assert Storage.kv_get(ets, "ns", "key1") == nil
    assert Storage.kv_get(ets, "ns", "key1", :default) == :default

    Storage.kv_put(ets, "ns", "key1", "value1")
    assert Storage.kv_get(ets, "ns", "key1") == "value1"

    # setting a different key doesn't clobber a value
    Storage.kv_put(ets, "ns", "key2", "value2")
    assert Storage.kv_get(ets, "ns", "key2") == "value2"
    assert Storage.kv_get(ets, "ns", "key1") == "value1"

    # setting a key in a different namespace doesn't clobber a value
    Storage.kv_put(ets, "ns2", "key1", "value3")
    assert Storage.kv_get(ets, "ns2", "key1") == "value3"
    assert Storage.kv_get(ets, "ns", "key1") == "value1"

    Storage.kv_del(ets, "ns", "key1")
    assert Storage.kv_get(ets, "ns", "key1") == nil

    Storage.close(ets)
  end
end
