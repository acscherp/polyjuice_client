# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.FilterTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Filter
  alias Polyjuice.Client.Filter

  test "create filters" do
    assert Filter.include_presence_types([]) == %{"presence" => %{"types" => []}}

    assert Filter.include_presence_types(["foo"])
           |> Filter.include_presence_types(["bar"]) == %{
             "presence" => %{"types" => ["foo", "bar"]}
           }

    assert Filter.exclude_presence_types([]) == %{"presence" => %{"not_types" => []}}

    assert Filter.exclude_presence_types(["foo"])
           |> Filter.exclude_presence_types(["bar"]) == %{
             "presence" => %{"not_types" => ["foo", "bar"]}
           }

    assert Filter.include_presence_types(["foo.*"])
           |> Filter.exclude_presence_types(["foo.bar"]) ==
             %{
               "presence" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }

    assert Filter.exclude_presence_types(["foo.bar"])
           |> Filter.include_presence_types(["foo.*"]) ==
             %{
               "presence" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }

    assert Filter.include_ephemeral_types([]) == %{"room" => %{"ephemeral" => %{"types" => []}}}

    assert Filter.include_ephemeral_types(["foo"])
           |> Filter.include_ephemeral_types(["bar"]) ==
             %{
               "room" => %{
                 "ephemeral" => %{"types" => ["foo", "bar"]}
               }
             }

    assert Filter.exclude_ephemeral_types([]) == %{
             "room" => %{"ephemeral" => %{"not_types" => []}}
           }

    assert Filter.exclude_ephemeral_types(["foo"])
           |> Filter.exclude_ephemeral_types(["bar"]) ==
             %{
               "room" => %{
                 "ephemeral" => %{"not_types" => ["foo", "bar"]}
               }
             }

    assert Filter.include_ephemeral_types(["foo.*"])
           |> Filter.exclude_ephemeral_types(["foo.bar"]) == %{
             "room" => %{
               "ephemeral" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.exclude_ephemeral_types(["foo.bar"])
           |> Filter.include_ephemeral_types(["foo.*"]) == %{
             "room" => %{
               "ephemeral" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.include_state_types([]) == %{"room" => %{"state" => %{"types" => []}}}

    assert Filter.include_state_types(["foo"])
           |> Filter.include_state_types(["bar"]) == %{
             "room" => %{
               "state" => %{"types" => ["foo", "bar"]}
             }
           }

    assert Filter.exclude_state_types([]) == %{"room" => %{"state" => %{"not_types" => []}}}

    assert Filter.exclude_state_types(["foo"])
           |> Filter.exclude_state_types(["bar"]) == %{
             "room" => %{
               "state" => %{"not_types" => ["foo", "bar"]}
             }
           }

    assert Filter.include_state_types(["foo.*"])
           |> Filter.exclude_state_types(["foo.bar"]) == %{
             "room" => %{
               "state" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.exclude_state_types(["foo.bar"])
           |> Filter.include_state_types(["foo.*"]) == %{
             "room" => %{
               "state" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.include_timeline_types(["foo.*"])
           |> Filter.exclude_timeline_types(["foo.bar"]) == %{
             "room" => %{
               "timeline" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.exclude_timeline_types(["foo.bar"])
           |> Filter.include_timeline_types(["foo.*"]) == %{
             "room" => %{
               "timeline" => %{"not_types" => ["foo.bar"], "types" => ["foo.*"]}
             }
           }

    assert Filter.limit_timeline_events(10) == %{"room" => %{"timeline" => %{"limit" => 10}}}

    assert Filter.lazy_loading() == %{
             "room" => %{
               "state" => %{"lazy_load_members" => true},
               "timeline" => %{"lazy_load_members" => true}
             }
           }

    assert Filter.limit_timeline_events(10) |> Filter.lazy_loading() == %{
             "room" => %{
               "state" => %{"lazy_load_members" => true},
               "timeline" => %{"lazy_load_members" => true, "limit" => 10}
             }
           }

    assert Filter.lazy_loading() |> Filter.limit_timeline_events(10) == %{
             "room" => %{
               "state" => %{"lazy_load_members" => true},
               "timeline" => %{"lazy_load_members" => true, "limit" => 10}
             }
           }
  end

  test "event filter" do
    assert Filter.Event.add_to_list("foo", ["bar"]) == %{"foo" => ["bar"]}

    assert Filter.Event.include_types(["m.*"])
           |> Filter.Event.exclude_types(["m.reaction"]) == %{
             "types" => ["m.*"],
             "not_types" => ["m.reaction"]
           }

    assert Filter.Event.exclude_types(["m.reaction"])
           |> Filter.Event.include_types(["m.*"]) == %{
             "types" => ["m.*"],
             "not_types" => ["m.reaction"]
           }

    assert Filter.Event.include_senders(["@alice:example.org"])
           |> Filter.Event.exclude_senders(["@bob:example.org"]) == %{
             "senders" => ["@alice:example.org"],
             "not_senders" => ["@bob:example.org"]
           }

    assert Filter.Event.include_senders(["@alice:example.org"])
           |> Filter.Event.include_senders(["@bob:example.org"]) == %{
             "senders" => ["@alice:example.org", "@bob:example.org"]
           }

    assert Filter.Event.exclude_senders(["@bob:example.org"])
           |> Filter.Event.include_senders(["@alice:example.org"]) == %{
             "senders" => ["@alice:example.org"],
             "not_senders" => ["@bob:example.org"]
           }

    assert Filter.Event.include_rooms(["!room1"])
           |> Filter.Event.exclude_rooms(["!room2"]) == %{
             "rooms" => ["!room1"],
             "not_rooms" => ["!room2"]
           }

    assert Filter.Event.exclude_rooms(["!room2"])
           |> Filter.Event.include_rooms(["!room1"]) == %{
             "rooms" => ["!room1"],
             "not_rooms" => ["!room2"]
           }

    assert Filter.Event.limit(10) == %{"limit" => 10}

    assert Filter.Event.contains_url(true) == %{"contains_url" => true}
  end
end
