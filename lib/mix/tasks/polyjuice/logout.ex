# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Mix.Tasks.Polyjuice.Logout do
  @moduledoc """
  Log out of a Matrix homeserver.

      mix polyjuice.logout [opts] homeserver_url

  ## Command line options

  * `--storage` - Elixir code to create the storage to fetch the client state
    from.
  * `--access-token` - The access token to log out.

  At least one of `--storage` or `--access-token` must be provided.

  """
  @shortdoc "Log out of a Matrix homeserver."
  use Mix.Task

  @impl Mix.Task
  def run(args) do
    Mix.Task.run("app.start", [])

    with {opts, [url]} <-
           OptionParser.parse!(args,
             strict: [
               storage: :string,
               access_token: :string
             ]
           ) do
      storage = opts[:storage]
      access_token = opts[:access_token]

      if {storage, access_token} == {nil, nil} do
        Mix.Task.run("help", ["polyjuice.logout"])
      else
        storage =
          if is_binary(storage) do
            Code.eval_string(storage) |> (fn {x, _} -> x end).()
          else
            nil
          end

        {:ok, client_pid} =
          Polyjuice.Client.start_link(
            url,
            access_token: opts[:access_token],
            storage: storage,
            sync: false
          )

        client = Polyjuice.Client.get_client(client_pid)

        ret = Polyjuice.Client.log_out(client)

        Polyjuice.Client.API.stop(client)

        if storage, do: Polyjuice.Client.Storage.close(storage)

        case ret do
          :ok ->
            IO.puts("Logout successful.")

          _ ->
            IO.puts("Logout failed: #{inspect(ret)}.")
        end
      end
    else
      _ ->
        Mix.Task.run("help", ["polyjuice.logout"])
    end
  end
end
