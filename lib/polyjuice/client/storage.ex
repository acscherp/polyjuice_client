# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defprotocol Polyjuice.Client.Storage do
  @moduledoc """
  Persistent storage for the Matrix client
  """

  @typedoc """
  Safe values to use for key-value storage.  All storage must support at least
  these types.
  """
  @type value() ::
          true
          | false
          | nil
          | String.t()
          | float()
          | [value(), ...]
          | %{optional(String.t()) => value()}

  @doc """
  Close the storage.
  """
  @spec close(storage :: __MODULE__.t()) :: any
  def close(storage)

  @doc """
  Get the latest sync token, or `nil` if no sync token has been set.
  """
  @spec get_sync_token(storage :: __MODULE__.t()) :: String.t() | nil
  def get_sync_token(storage)

  @doc """
  Set the sync token.
  """
  @spec set_sync_token(storage :: __MODULE__.t(), token :: String.t()) :: any
  def set_sync_token(storage, token)

  @doc """
  Store the ID for a filter.
  """
  @spec set_filter_id(storage :: __MODULE__.t(), filter :: map, filter_id :: String.t()) :: any
  def set_filter_id(storage, filter, filter_id)

  @doc """
  Get the ID stored for a filter, or `nil` if no ID has been stored.
  """
  @spec get_filter_id(storage :: __MODULE__.t(), filter :: map) :: String.t() | nil
  def get_filter_id(storage, filter)

  @doc """
  Store data for a specific key.
  """
  @spec kv_put(__MODULE__.t(), String.t(), String.t(), __MODULE__.value()) :: any
  def kv_put(storage, namespace, key, value)

  @doc """
  Get the data for a specific key.
  """
  @spec kv_get(__MODULE__.t(), String.t(), String.t(), __MODULE__.value()) :: __MODULE__.value()
  def kv_get(storage, namespace, key, default \\ nil)

  @doc """
  Delete the data for a specific key.
  """
  @spec kv_del(__MODULE__.t(), String.t(), String.t()) :: any
  def kv_del(storage, namespace, key)
end
