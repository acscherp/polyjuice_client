# Copyright 2021 Arjan Scherpenisse <arjan@miraclethings.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsTyping do
  @moduledoc """
  Updates the typing indicator for a user

  https://matrix.org/docs/spec/client_server/r0.6.1#put-matrix-client-r0-rooms-roomid-typing-userid
  """

  @type t :: %__MODULE__{
          room: String.t(),
          user_id: String.t(),
          typing: boolean(),
          timeout: non_neg_integer() | nil
        }

  @enforce_keys [:room, :user_id, :typing]
  defstruct [
    :room,
    :user_id,
    :typing,
    :timeout
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PutRoomsTyping{
          room: room,
          user_id: user_id,
          typing: typing,
          timeout: timeout
        }) do
      e = &URI.encode_www_form/1

      body =
        case timeout do
          nil -> %{typing: typing}
          _ -> %{typing: typing, timeout: timeout}
        end
        |> Jason.encode!()

      Polyjuice.Client.Endpoint.HttpSpec.put(
        :r0,
        "rooms/#{e.(room)}/typing/#{e.(user_id)}",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, _parsed) do
      :ok
    end
  end
end
