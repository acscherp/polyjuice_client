# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostRoomsReadMarkers do
  @moduledoc """
  Sets the position of the read marker for a given room, and optionally the read receipt's location.
  https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-rooms-roomid-read-markers
  """

  @type t :: %__MODULE__{
          room: String.t(),
          fully_read: String.t(),
          read: String.t() | nil
        }

  @enforce_keys [:room, :fully_read]

  defstruct [
    :room,
    :fully_read,
    :read
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PostRoomsReadMarkers{
          room: room,
          fully_read: fully_read,
          read: read
        }) do
      e = &URI.encode_www_form/1

      query =
        case read do
          nil -> %{"m.fully_read": fully_read}
          _ -> %{"m.fully_read": fully_read, "m.read": read}
        end

      {:ok, body} = query |> Jason.encode()

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "rooms/#{e.(room)}/read_markers",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, _body) do
      :ok
    end
  end
end
