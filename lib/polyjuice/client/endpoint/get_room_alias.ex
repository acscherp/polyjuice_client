# Copyright 2021 Arjan Scherpenisse <arjan@miraclethings.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomAlias do
  @moduledoc """
  Retrieve the mapping from room alias to room ID.
  https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-directory-room-roomalias
  """

  @type t :: %__MODULE__{
          room_alias: String.t()
        }

  @enforce_keys [:room_alias]

  defstruct [
    :room_alias
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.GetRoomAlias{
          room_alias: room_alias
        }) do
      Polyjuice.Client.Endpoint.HttpSpec.get(
        :r0,
        "directory/room/#{URI.encode_www_form(room_alias)}",
        auth_required: false
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, %{"room_id" => room_id} = parsed) do
      {:ok, {room_id, Map.delete(parsed, "room_id")}}
    end
  end
end
